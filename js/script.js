function Stopwatch(elem) {
    let time = 0;
    let interval;
    let offset;

    function update() {
        if(this.isOn) {
            time += delta();
        }
        let formattedTime = timeFormatter(time);
        elem.textContent = formattedTime;
    };

    function delta() {
        let now = Date.now();
        let timePassed = now - offset;
        offset = now;
        return timePassed;
    };

    function timeFormatter(timeInMilliseconds) {
        let time = new Date(timeInMilliseconds);
        let minutes = time.getMinutes().toString();
        let seconds = time.getSeconds().toString();
        let milliseconds = time.getMilliseconds().toString();

        if(minutes.length < 2){
            minutes = '0' + minutes;
        }

        if(seconds.length < 2){
            seconds = '0' + seconds;
        }

        while(milliseconds.length < 3){
            milliseconds = '0' + milliseconds;
        }

        return minutes + ' : ' + seconds + ' . ' + milliseconds;
    };

    this.isOn = false;

    this.start = function() {
        if(!this.isOn){
            interval = setInterval(update.bind(this),10);
            offset = Date.now();
            this.isOn = true;
        }
    };

    this.stop = function() {
        if(this.isOn){
            clearInterval(interval);
            interval = null;
            this.isOn = false;
        }
    };
    this.reset = function() {
        time = 0;
        update();
    };
     
};

let timer = document.getElementById('timer');
let toggleBtn = document.getElementById('toggle');
let resetBtn = document.getElementById('reset');

let watch = new Stopwatch(timer);

toggleBtn.addEventListener('click', function() {
    if(watch.isOn) {
        watch.stop();
        toggleBtn.textContent = 'Start';
    } else {
        watch.start();
        toggleBtn.textContent = 'Pause';
    }
});

resetBtn.addEventListener('click', function() {
    watch.stop();
    watch.reset();
    toggleBtn.textContent = 'Start';
});